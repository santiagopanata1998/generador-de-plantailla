import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searhList'
})
export class SearhListPipe implements PipeTransform {

  transform(items:any[], term:string, key:string): any[] {
    console.log(term,items)
    if(!items || !term){
      return items
    }
    
    term = term.toLowerCase().trim();
    return items.filter(item=>item[key].toLowerCase().includes(term))

  }

}
