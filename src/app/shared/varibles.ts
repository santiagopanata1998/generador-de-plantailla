export const classStyle: string = `
<style>
/* Texto pequeño */
.text-tiny {
    font-size: 10px !important;
    /* Puedes ajustar el tamaño de fuente según tus necesidades */
}

/* Texto pequeño */
.text-small {
    font-size: 12px !important;
    /* Puedes ajustar el tamaño de fuente según tus necesidades */
}

/* Texto grande */
.text-big {
    font-size: 16px !important;
    /* Puedes ajustar el tamaño de fuente según tus necesidades */
}

/* Texto enorme */
.text-huge {
    font-size: 24px !important;
    /* Puedes ajustar el tamaño de fuente según tus necesidades */
}


.marker-yellow {
    background-color: #fdfd77;
    border-radius: 2px;
}
.marker-green {
    background-color: #62f962;
    border-radius: 2px;
}
.marker-pink {
    background-color: #fc7899;
    border-radius: 2px;
}
.marker-blue {
    background-color: #72ccfd;
    border-radius: 2px;
}
.marker-red {
    background-color: #72ccfd;
    border-radius: 2px;
}
.pen-red {
    color: #e71313;
    border-radius: 2px;
}
.pen-green {
    color: #128a00;
    border-radius: 2px;
}


.table {
    border-collapse: collapse; 
  }
  
  .table th, .table td {
    border: 1px solid #000; 
  }
  
  .table th {
    background-color: #f2f2f2; 
  }
  </style>

`
