import { NgModule } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import { SearhListPipe } from './pipes/searh-list.pipe';
@NgModule({
  declarations: [
    SearhListPipe
  ],
  imports: [
    CommonModule,
    CKEditorModule,
    MatDialogModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule, 
    NgFor
    
  ],
  exports: [
    CommonModule,
    CKEditorModule,
    MatDialogModule,
    MatRadioModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule, 
    NgFor,
    SearhListPipe

  ]
})
export class SharedModule { }
