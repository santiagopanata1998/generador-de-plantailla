export interface VariblesGetI {
    workflowId:number
}


export interface FetchVariablesI {
    data:    VariablesI[];
    message: string;
}

export interface VariablesI {
    logos:            Logo[];
    listadoVariables: ListadoVariable[];
    fechaCertificado: FechaCertificado;
}

export interface FechaCertificado {
    created_at: string;
    updated_at: string;
}

export interface ListadoVariable {
    key:      string;
    datatype: string;
}


export interface Logo {
    name:  string;
    code:  string;
    value: string;
}

///Guardar la plantilla HTML 

export interface Plantilla {
    html:string;
}
