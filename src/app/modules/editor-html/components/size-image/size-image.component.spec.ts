import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SizeImageComponent } from './size-image.component';

describe('SizeImageComponent', () => {
  let component: SizeImageComponent;
  let fixture: ComponentFixture<SizeImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SizeImageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SizeImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
