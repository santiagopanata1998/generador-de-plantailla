import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-size-image',
  templateUrl: './size-image.component.html',
  styleUrls: ['./size-image.component.css'],
  standalone: true,
  imports: [SharedModule],
})
export class SizeImageComponent {
  myForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<SizeImageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) {

  }
  ngOnInit(): void {
    this.myForm = this.createForm()
  }

  createForm() {
    return this.fb.group({
      altura: [20, Validators.required],
      ancho: [20, Validators.required],
      alternativo: ['Imagen', Validators.required],
    })
  }

  saveTamano() {
    const text: string = '<img src="{{ ' + this.data + ' }}" alt="' + this.myForm.get('alternativo')?.value + '" width="' + this.myForm.get('ancho')?.value + '" height="' + this.myForm.get('altura')?.value + '">'
    this.dialogRef.close(text)
  }
}
