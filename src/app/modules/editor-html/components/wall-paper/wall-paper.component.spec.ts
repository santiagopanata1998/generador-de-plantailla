import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WallPaperComponent } from './wall-paper.component';

describe('WallPaperComponent', () => {
  let component: WallPaperComponent;
  let fixture: ComponentFixture<WallPaperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WallPaperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WallPaperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
