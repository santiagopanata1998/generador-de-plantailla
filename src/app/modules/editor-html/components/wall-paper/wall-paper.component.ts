import { NgFor } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  selector: 'app-wall-paper',
  templateUrl: './wall-paper.component.html',
  styleUrls: ['./wall-paper.component.css'],
  standalone: true,
  imports: [SharedModule],
})
export class WallPaperComponent implements OnInit {


  myForm!: FormGroup;
  public image: boolean = false


  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<WallPaperComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {

  }
  ngOnInit(): void {
    this.myForm = this.createForm()
    this.changeValue()
  }

  changeValue() {
    this.myForm.get('tipo')?.valueChanges.subscribe((data) => {
      if (data) {
        this.image = false
        if (data == 'I') {
          this.myForm.get('image')?.enable()
          this.myForm.get('image_style')?.enable()
          this.myForm.get('text')?.disable()

          this.image = true
        }
        if (data == 'T') {
          this.myForm.get('text')?.enable()
          this.myForm.get('image')?.disable()
          this.myForm.get('image_style')?.disable()
        }

      }
    })
  }

  createForm() {
    return this.fb.group({
      tipo: ['T', Validators.required],
      text: ['', Validators.required],
      image: ['', Validators.required],
      image_style: ['', Validators.required],
    })
  }

  option(value: string) {
    this.myForm.get('image_style')?.setValue(value)
  }

  styleImage(value: string) {
    return { 'border border-secondary': this.myForm.get('image_style')?.value == value }
  }







}
