import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FetchVariablesI, Plantilla, VariblesGetI } from '../interfaces/editor.interfaces';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

const token = environment.token
@Injectable({
  providedIn: 'root'
})
export class TempleteService {

  constructor(
    private http: HttpClient
  ) { }

  getVariable(url:string ,data: VariblesGetI): Observable<FetchVariablesI> {
    return this.http.post<FetchVariablesI>(url, data, { headers: token })
  }

  saveTemplate(data: Plantilla):Observable<any> {
    
    return this.http.post<any>(`${environment.urlAdress}certificate/save-certificate`,data,{headers:token})
  }
}
