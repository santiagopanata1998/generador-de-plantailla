import { TestBed } from '@angular/core/testing';

import { TempleteService } from './templete.service';

describe('TempleteService', () => {
  let service: TempleteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TempleteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
