import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexEditorHtmlComponent } from './pages/index-editor-html/index-editor-html.component';

const routes: Routes = [
  {path:'editor-html', component:IndexEditorHtmlComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditorHtmlRoutingModule { }
