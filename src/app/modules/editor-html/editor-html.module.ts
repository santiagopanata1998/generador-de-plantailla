import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditorHtmlRoutingModule } from './editor-html-routing.module';
import { IndexEditorHtmlComponent } from './pages/index-editor-html/index-editor-html.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { WallPaperComponent } from './components/wall-paper/wall-paper.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SizeImageComponent } from './components/size-image/size-image.component';


@NgModule({
  declarations: [
    IndexEditorHtmlComponent,
    
    SizeImageComponent
  ],
  imports: [
    CommonModule,
    EditorHtmlRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    SharedModule,
  ]
})
export class EditorHtmlModule { }
