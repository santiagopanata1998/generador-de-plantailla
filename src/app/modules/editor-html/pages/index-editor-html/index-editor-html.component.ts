import { Component } from '@angular/core';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
const trama = {
  "data": [
    {
      "key": "nombre",
      "dataType": "string"
    },
    {
      "key": "fecha",
      "dataType": "date"
    },
    {
      "key": "logo_arcsa",
      "dataType": "img"
    },
    {
      "key": "actividades",
      "dataType": "array"
    }
  ],
  "message": "Transacción realizada con éxito"
}
@Component({
  selector: 'app-index-editor-html',
  templateUrl: './index-editor-html.component.html',
  styleUrls: ['./index-editor-html.component.css']
})
export class IndexEditorHtmlComponent {
  title = 'template';
  public editor = Editor;
  public editorInstance: any;
  public varibales = trama;
  public variables: boolean = false;
  public texto = '<p style="text-align:center;">&lt;img src="{{ logo_arcsa }}" alt="Mi Imagen"&gt;</p><p style="text-align:right;">{{fecha}}</p><p>Querido {{nombre}}, tiene las siguientes actividades&nbsp;</p><p>&lt;ul&gt;{{#each actividades}}&lt;li&gt;{{this.nombre}}&lt;/li&gt;{{/each}} &lt;/ul&gt;</p>'

  public config = {
    toolbar: {
      items: [
        'heading',
        '|',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'link',
        '|',
        'bulletedList',
        'numberedList',
        'blockQuote',
        '|',
        'alignment',
        'indent',
        'outdent',
        '|',
        'insertTable',
        'imageUpload',
        'mediaEmbed',
        '|',
        'undo',
        'redo'
      ]
    },
  }

  viewBtn() {
    this.variables = true;
  }

  public onReady(editor: any) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    // Guarda la instancia del editor para su uso posterior
    this.editorInstance = editor;
    // Agregar un listener al editor para detectar selecciones,

    this.editorInstance.editing.view.document.on('selectionChange', () => {

    });
  }

  markSelectedWord() {
    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const selection = model.document.selection;

      if (selection.isCollapsed) {
        return;
      }

      const ranges: any = Array.from(selection.getRanges());
      for (const range of ranges) {
        const markerElement = model.createElement('span');
        markerElement.addClass('delete-marker');
        markerElement.setAttribute('data-marker', 'true');
        markerElement.setHtml('X');

        model.change((writer: any) => {
          writer.insert(markerElement, range.start);
        });
      }
    }
  }

  typeVarible(data: { "key": string, "dataType": string }) {
    let text = ''
    if (data.dataType == 'string') {
      text = '{{' + data.key + '}}'
    }
    if (data.dataType == 'date') {
      text = '{{' + data.key + '}}'
    }
    if (data.dataType == 'img') {
      text = '<img src="{{ ' + data.key + ' }}" alt="Mi Imagen">'
    }
    if (data.dataType == 'array') {
      text = '<ul>{{#each ' + data.key + '}}<li>{{this}}</li>{{/each}} </ul>'
    }
    this.insertTextAtCursor(text)
  }

  inserObject(data: string) {
    let text = '{{' + data + '}}'
    this.insertTextAtCursor(text)
  }

  inserArray(data: string, key: string) {
    let text = '<ul>{{#each ' + data + '}}<li>{{this.' + key + '}}</li>{{/each}} </ul>'
    this.insertTextAtCursor(text)
  }

  insertHTML() {
    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const cursorPosition = model.document.selection.getFirstPosition();
      const htmlToInsert = '<p>Contenido HTML a insertar.</p>';

      model.change((writer: any) => {
        writer.insertHtml(htmlToInsert, cursorPosition);
      });
    }
  }

  public insertTextAtCursor(text: string) {

    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const cursorPosition = model.document.selection.getFirstPosition();
      model.change((writer: any) => {
       
        writer.insertText(text, cursorPosition);
      });
      // Desplazar el enfoque al final del contenido
      const editableElement = this.editorInstance.ui.getEditableElement();
      editableElement.focus();

      const range = document.createRange();
      range.selectNodeContents(editableElement);
      range.collapse(false);

      const selection = window.getSelection()!;
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }

  public getEditorHtml(): string {
    if (this.editorInstance) {
      return this.editorInstance.getData()
      //.replace(/&lt;/g,'<').replace(/&gt;/g,'>'); // Obtiene el contenido HTML
    }
    return ''; // Retorna una cadena vacía si no hay instancia del editor
  }



}
