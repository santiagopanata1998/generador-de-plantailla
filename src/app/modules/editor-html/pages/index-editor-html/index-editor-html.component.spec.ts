import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexEditorHtmlComponent } from './index-editor-html.component';

describe('IndexEditorHtmlComponent', () => {
  let component: IndexEditorHtmlComponent;
  let fixture: ComponentFixture<IndexEditorHtmlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndexEditorHtmlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IndexEditorHtmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
