import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Editor from 'ckeditor5-custom-build/build/ckeditor';
import { TempleteService } from './modules/editor-html/services/templete.service';
import { finalize } from 'rxjs';
import { FetchVariablesI } from './modules/editor-html/interfaces/editor.interfaces';
import { MatDialog } from '@angular/material/dialog';
import { WallPaperComponent } from './modules/editor-html/components/wall-paper/wall-paper.component';
import { MatButtonModule } from '@angular/material/button';
import { SizeImageComponent } from './modules/editor-html/components/size-image/size-image.component';
import { classStyle } from './shared/varibles';
import { environment } from 'src/environments/environment.development';
import { CKEditorComponent } from '@ckeditor/ckeditor5-angular';
const trama = {
  "data": [
    {
      "key": "nombre",
      "dataType": "string"
    },
    {
      "key": "fecha",
      "dataType": "date"
    },
    {
      "key": "logo_arcsa",
      "dataType": "img"
    },
    {
      "key": "actividades",
      "dataType": "array"
    }
  ],
  "message": "Transacción realizada con éxito"
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit {

  @ViewChild('paginaContainer', { static: false }) paginaContainer!: ElementRef;
  @ViewChild('editor', { static: false }) editorComponent!: CKEditorComponent;

  public viewBtnSave: boolean = environment.viewSave
  public altura: number = 20
  public ancho: number = 20
  public form!: FormGroup
  public loadingSave: boolean = false
  public loadingVariables: boolean = false
  public alertSave: string = ''
  public alertVariables: string = ''

  public listIcons: FormControl = new FormControl('')
  public listVariables: FormControl = new FormControl('')


  public response: any
  // {
  //   "data": [
  //     {
  //       "logos": [
  //         {
  //           "name": "Logo app",
  //           "code": "APP_LOGO",
  //           "value": "configurations/1530009088623824.png"
  //         },
  //         {
  //           "name": "Logo splash app",
  //           "code": "APP_SPLASH_LOGO",
  //           "value": "configurations/9719351769577408.svg"
  //         },
  //         {
  //           "name": "Logo Cuaternario",
  //           "code": "FOURTH_LOGO",
  //           "value": "configurations/5266395914932154.png"
  //         },
  //         {
  //           "name": "Firma electronica",
  //           "code": "FIRMA_ELECTRONICA",
  //           "value": "configurations/3004609134904927.png"
  //         },
  //         {
  //           "name": "Logo MSP",
  //           "code": "MSP_ARCSA_LOGO",
  //           "value": "configurations/7518543300161325.png"
  //         },
  //         {
  //           "name": "Logo email",
  //           "code": "MAIL_LOGO",
  //           "value": "configurations/1543564884593866.png"
  //         },
  //         {
  //           "name": "Logo Landing Secondary",
  //           "code": "SECONDARY_LANDING_LOGO",
  //           "value": "configurations/7499385385940389.png"
  //         },
  //         {
  //           "name": "Logo Landing Principal",
  //           "code": "PRIMARY_LANDING_LOGO",
  //           "value": "configurations/5934213795008616.png"
  //         },
  //         {
  //           "name": "Logo Terciario",
  //           "code": "THIRD_LOGO",
  //           "value": "configurations/6111639302190170.png"
  //         },
  //         {
  //           "name": "Logo Secundario",
  //           "code": "SECONDARY_LOGO",
  //           "value": "configurations/3576330892637739.png"
  //         },
  //         {
  //           "name": "Logo Principal",
  //           "code": "PRIMARY_LOGO",
  //           "value": "configurations/0371396982710552.svg"
  //         }
  //       ],
  //       "listadoVariables": [
  //         {
  //           "key": "id",
  //           "datatype": "numero"
  //         },
  //         {
  //           "key": "answer_id_783495",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "area",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "cat_empresa",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "cedula_representante_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "correo_de_representante_legal",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "correo_electronico_alterno_del_contacto",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "doc_anexos",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "numero_permiso_funcionamiento_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "referencia_ubicacion",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "si_no",
  //           "datatype": "numero"
  //         },
  //         {
  //           "key": "telefono_representante_legal",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "tipo_de_establecimiento_clv_ext",
  //           "datatype": "numero"
  //         },
  //         {
  //           "key": "tipo_laboratorio",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "ubicacion_geografica",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "zonas_seleccionar",
  //           "datatype": "numero"
  //         },
  //         {
  //           "key": "created_at_785678",
  //           "datatype": "timestamp without time zone"
  //         },
  //         {
  //           "key": "user_create_325464",
  //           "datatype": "numero"
  //         },
  //         {
  //           "key": "status_197382",
  //           "datatype": "boolean"
  //         },
  //         {
  //           "key": "canton",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "codigo_bpm",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "direccion",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "establecimiento",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "nombre_comercial",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "parroquia",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "provincia",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "razon_social",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "ruc",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "nombre_representante_legal",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "tipo_establecimiento",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "correo_electronico_del_contacto",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "ced_direct_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "cedula_representante_legal",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "corr_direct_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "nombre_director_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "nombre_responsable_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "telf_direct_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "canton_ciudad_empresa_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "codigo_bpm_anterior",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "correo_representante_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "direccion_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "establecimiento_fabricante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "fecha_vigencia_certificado",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "nombre_razon_social_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "parroquia_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "provincia_solicitante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "telefono_representante_tecnico",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "anexo_1",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "anexo_2",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "detalle_producto",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "area_de_adt",
  //           "datatype": "json"
  //         },
  //         {
  //           "key": "zona",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "canton_ciudad_fabricante",
  //           "datatype": "string"
  //         },
  //         {
  //           "key": "exportador_clv_externa",
  //           "datatype": "numero"
  //         }
  //       ],
  //       "fechaCertificado": {
  //         "created_at": "29 de junio de 2023 14:39",
  //         "updated_at": "29 de junio de 2023 14:39"
  //       }
  //     }
  //   ],
  //   "message": "Transacción realizada con éxito"
  // }


  title = 'template';
  public editor = Editor;
  public editorInstance: any;
  public varibales = trama;
  public variables: boolean = false;
  contenidoHTML: string = '  <figure ><table><tbody><tr><td>Nombe</td><td>Apellido</td><td>Edad</td><td>Habilidad</td></tr><tr><td>Edgar Santago</td><td>Panata Castillo</td><td>24 años</td><td>Desarrollo de software</td></tr></tbody></table></figure><p>&nbsp;</p><p>&nbsp;</p> <figure class="table"><table><tbody><tr><td>Nombe</td><td>Apellido</td><td>Edad</td><td>Habilidad</td></tr><tr><td>Edgar Santago</td><td>Panata Castillo</td><td>24 años</td><td>Desarrollo de software</td></tr></tbody></table></figure><p>&nbsp;</p><p>&nbsp;</p>';

  constructor(
    private sanitizer: DomSanitizer,
    private renderer: Renderer2,
    public dialog: MatDialog,
    private templateService: TempleteService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.create()
    this.changeValues()

  }

  changeValues() {
    this.form.get('url')?.valueChanges.subscribe((data) => {
      this.form.get('nombre')?.reset()
      this.variables = false
      this.resetEditor()
    })
  }


  // Restablecer el contenido del editor a un valor específico
  resetEditor() {
    this.texto = ''; // Puedes establecer el contenido en blanco u otro valor predeterminado
    //this.editorComponent?.editorInstance?.setData(this.texto);
    this.editorInstance.setData(this.texto); // Restablece el contenido del editor
  }

  create() {
    return this.fb.group({
      url: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
    })
  }

  get contenidoSeguro(): SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(this.getEditorHtml());
  }
  // public texto = '<p style="text-align:center;">&lt;img src="{{ logo_arcsa }}" alt="Mi Imagen"&gt;</p><p style="text-align:right;">{{fecha}}</p><p>Querido {{nombre}}, tiene las siguientes actividades&nbsp;</p><p>&lt;ul&gt;{{#each actividades}}&lt;li&gt;{{this.nombre}}&lt;/li&gt;{{/each}} &lt;/ul&gt;</p>'
  public texto = ''
  public config = {
    toolbar: {
      items: [
        'heading',
        '|',
        'bold',
        'italic',
        'underline',
        'strikethrough',
        'link',
        '|',
        'bulletedList',
        'numberedList',
        'blockQuote',
        '|',
        'alignment',
        'indent',
        'outdent',
        '|',
        'insertTable',
        'imageUpload',
        'mediaEmbed',
        '|',
        'undo',
        'redo'
      ]
    },
  }

  viewBtn() {
    const value: string = this.form.get('url')?.value || ''

    this.templateService.getVariable(value, { workflowId: 81 }).pipe(
      finalize(() => {
        setTimeout(
          () => { this.alertVariables = '' }
          , 2000);
        this.loadingVariables = false
      }
      )
    ).subscribe(
      {
        next: (resp) => {
          this.variables = true;
          this.loadingVariables = true;
          this.listadoBtns()
          this.response = resp
          this.nameTemplate(value)
        }, error: (error: any) => {
          this.alertVariables = 'Problemas al consultar, inténtalo mas tarde.'
        }
      }
    )
  }


  nameTemplate(value: string) {
    const valueDes: string[] = value.split('/') || []
    this.form.get('nombre')?.setValue(valueDes[valueDes.length - 1])
  }

  /**
   * Enviar a guardar el templete en la base de datos
   */
  saveTemplate() {
    const dataHtml = {
      html: '<html>' + classStyle + '<body>' + this.getEditorHtml() + '</body></html>'
    }
    this.loadingSave = true
    this.templateService.saveTemplate(dataHtml).pipe(
      finalize(() => {
        setTimeout(
          () => { this.alertSave = '' }
          , 2000);
        this.loadingSave = false
      })
    ).subscribe(
      {
        next: (data) => {
          this.alertSave = 'success'
          console.log(data)
        },
        error: () => {
          this.alertSave = 'danger'
        }
      }
    )
  }
  public onReady(editor: any) {
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    // Guarda la instancia del editor para su uso posterior
    this.editorInstance = editor;
    // Agregar un listener al editor para detectar selecciones,

    this.editorInstance.editing.view.document.on('selectionChange', () => {

    });
  }

  markSelectedWord() {
    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const selection = model.document.selection;

      if (selection.isCollapsed) {
        return;
      }

      const ranges: any = Array.from(selection.getRanges());
      for (const range of ranges) {
        const markerElement = model.createElement('span');
        markerElement.addClass('delete-marker');
        markerElement.setAttribute('data-marker', 'true');
        markerElement.setHtml('X');

        model.change((writer: any) => {
          writer.insert(markerElement, range.start);
        });
      }
    }
  }

  typeVarible(data: { "key": string, "dataType": string }) {
    let text = ''
    if (data.dataType == 'string') {
      text = '{{' + data.key + '}}'
    }
    if (data.dataType == 'date') {
      text = '{{' + data.key + '}}'
    }
    if (data.dataType == 'img') {
      text = '<img src="{{ ' + data.key + ' }}" alt="Mi Imagen">'
    }
    if (data.dataType == 'array') {
      text = '<ul>{{#each ' + data.key + '}}<li>{{this}}</li>{{/each}} </ul>'
    }
    this.insertTextAtCursor(text)
  }

  inserObject(data: string) {
    let text = '{{' + data + '}}'
    this.insertTextAtCursor(text)
  }

  insertImage(data: string) {
    this.dialog.open(SizeImageComponent, {
      data: data
    }).afterClosed().subscribe((data) => {
      if (data) {
        this.insertTextAtCursor(data)
      }
    })
  }

  inserArray(data: string, key: string) {
    let text = '<ul>{{#each ' + data + '}}<li>{{this.' + key + '}}</li>{{/each}} </ul>'
    this.insertTextAtCursor(text)
  }

  insertHTML() {
    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const cursorPosition = model.document.selection.getFirstPosition();
      const htmlToInsert = '<p>Contenido HTML a insertar.</p>';

      model.change((writer: any) => {
        writer.insertHtml(htmlToInsert, cursorPosition);
      });
    }
  }

  public insertTextAtCursor(text: string) {

    if (this.editorInstance) {
      const model = this.editorInstance.model;
      const cursorPosition = model.document.selection.getFirstPosition();
      model.change((writer: any) => {

        writer.insertText(text, cursorPosition);
      });
      // Desplazar el enfoque al final del contenido
      const editableElement = this.editorInstance.ui.getEditableElement();
      editableElement.focus();

      const range = document.createRange();
      range.selectNodeContents(editableElement);
      range.collapse(false);

      const selection = window.getSelection()!;
      selection.removeAllRanges();
      selection.addRange(range);
    }
  }

  public getEditorHtml(): string {
    if (this.editorInstance) {
      this.contenidoHTML = this.editorInstance.getData()
      console.log(this.editorInstance.getData())
      this.contenidoPorPaginas(this.contenidoHTML)
      return this.editorInstance.getData()
      //.replace(/&lt;/g,'<').replace(/&gt;/g,'>'); // Obtiene el contenido HTML
    }
    return ''; // Retorna una cadena vacía si no hay instancia del editor
  }

  contenidoPorPaginas(text: string) {

    this.limpiarContenido()
    // Aquí, obtén tu contenido HTML dinámico y asígnalo a la variable 'contenidoHTML'

    // Divide el contenido en páginas A4
    const contenidoDiv = document.createElement('div');
    contenidoDiv.innerHTML = text;

    // Tamaño de una página A4 en píxeles (ajústalo según tus necesidades)
    const paginaA4Height = 842; // Altura en píxeles

    let paginaActual: HTMLElement | null = null;
    let paginaActualHeight = 0;
    let indes = 0
    if (!paginaActual) {
      paginaActual = document.createElement('div');
      this.renderer.addClass(paginaActual, 'dina4');
      this.renderer.appendChild(this.paginaContainer.nativeElement, paginaActual);
    }

    contenidoDiv.childNodes.forEach((element: ChildNode, index: number, array: NodeListOf<ChildNode>) => {



      if (paginaActual) {
        // paginaActual.appendChild(element);,
      }

      // if (paginaActualHeight + (element as HTMLElement).clientHeight > paginaA4Height) {
      //   // Si el elemento no cabe en la página actual, crea una nueva página
      //   paginaActual = document.createElement('div');
      //   this.renderer.addClass(paginaActual, 'dina4');
      //   this.renderer.appendChild(this.paginaContainer.nativeElement, paginaActual);
      //   paginaActualHeight = 0;
      // }


      // paginaActual.appendChild(element as HTMLElement);

      // paginaActualHeight += (element as HTMLElement).clientHeight;

    });
  }

  /**
   * Limpiar el contenido de la tabla
   */
  limpiarContenido() {
    if (this.paginaContainer) {
      const divElement = this.paginaContainer.nativeElement;
      divElement.innerHTML = ''; // Limpia el contenido del div
    }
  }


  public keys: string[] = []
  /**
   * Obtener las claves de los botones
   */
  listadoBtns() {
    this.keys = Object.keys(this.response?.data[0])


  }

  /**
   * Abrir el modal para la imagen de fondo
   */
  openModal() {
    this.dialog.open(WallPaperComponent)
  }

  /**
   * Abrir el modal para el tamaño
   */
  openModalTamano() {
    this.dialog.open(SizeImageComponent)
  }










}
